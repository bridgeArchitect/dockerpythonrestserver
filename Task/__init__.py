from flask import Flask, request, json
import os

app = Flask(__name__)
DB_FILE_NAME = '/user/src/app/shared_folder/users.txt'

# curl -X GET localhost:5000/users/IsLoginFree?login=admin2
@app.route('/users/IsLoginFree', methods = ['GET'])
def isLoginFree():
    if not os.path.exists(DB_FILE_NAME):
        return json.dumps(True)

    with open(DB_FILE_NAME) as f:
        content = [x.strip() for x in f.readlines()]

    return json.dumps(request.args.get('login') not in content)

# curl -X POST localhost:5000/users/AddLogin -d "{\"login\": \"admin\"}" -H "Content-Type: application/json; charset=UTF-8"
@app.route('/users/AddLogin', methods = ['POST'])
def addLogin():
    login = str(json.loads(request.data)['login'])

    if os.path.exists(DB_FILE_NAME):
        with open(DB_FILE_NAME) as f:
            content = [x.strip() for x in f.readlines()]
        if login in content:
            return 'This login already exists', 400

    with open(DB_FILE_NAME, 'a' if os.path.exists(DB_FILE_NAME) else 'w') as f:
        f.write(login + '\n')

    return json.dumps(login)

# curl -X POST localhost:5000/users/IsLoginFree?T=something&H=something
@app.route('/users/UpdateParameters', methods = ['POST'])
def updateParameters():
    login = str(request.args.get('login'))
    nlogin = str(request.args.get('nlogin'))

    ind = "no"
    with open(DB_FILE_NAME, "r") as f:
        lines = f.readlines()
    with open(DB_FILE_NAME, "w") as f:
        for line in lines:
            if line.strip("\n") != login:
                f.write(line)
            else:
                ind = "yes"
        if ind == "yes":
            f.write(nlogin + "\n")
    return ind
